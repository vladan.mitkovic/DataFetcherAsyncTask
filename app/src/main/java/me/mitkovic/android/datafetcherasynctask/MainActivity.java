package me.mitkovic.android.datafetcherasynctask;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import me.mitkovic.android.datafetcherasynctask.interfaceone.InterfaceOneActivity;
import me.mitkovic.android.datafetcherasynctask.interfacetwo.InterfaceTwoActivity;
import me.mitkovic.android.datafetcherasynctask.notcanceling.NotCancelledActivity;
import me.mitkovic.android.datafetcherasynctask.notstaticinnerclass.NotStaticInnerClassActivity;
import me.mitkovic.android.datafetcherasynctask.standalone.StandaloneActivity;
import me.mitkovic.android.datafetcherasynctask.staticinnerclass.StaticInnerClassActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.app_name));
        setSupportActionBar(toolbar);

        Button standaloneButton = (Button) findViewById(R.id.standaloneButton);
        standaloneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, StandaloneActivity.class));
            }
        });

        Button staticInnerClassButton = (Button) findViewById(R.id.staticInnerClassButton);
        staticInnerClassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, StaticInnerClassActivity.class));
            }
        });

        Button interfaceOneButton = (Button) findViewById(R.id.interfaceOneButton);
        interfaceOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, InterfaceOneActivity.class));
            }
        });

        Button interfaceTwoButton = (Button) findViewById(R.id.interfaceTwoButton);
        interfaceTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, InterfaceTwoActivity.class));
            }
        });

        Button notCanceledButton = (Button) findViewById(R.id.notCancelledButton);
        notCanceledButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, NotCancelledActivity.class));
            }
        });

        Button notStaticButton = (Button) findViewById(R.id.notStaticButton);
        notStaticButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, NotStaticInnerClassActivity.class));
            }
        });

    }
}
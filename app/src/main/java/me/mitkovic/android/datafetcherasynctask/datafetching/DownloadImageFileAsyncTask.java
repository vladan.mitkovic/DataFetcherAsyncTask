package me.mitkovic.android.datafetcherasynctask.datafetching;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class DownloadImageFileAsyncTask extends AsyncTask<String, Void, Bitmap> {

    private IDownloadImageAsyncTaskHolder mActivity;

    public DownloadImageFileAsyncTask(IDownloadImageAsyncTaskHolder activity) {
        mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mActivity != null) {
            mActivity.showProgressBar();
        }
    }

    @Override
    protected Bitmap doInBackground(String... param) {
        Bitmap bitmap = null;
        try {
            if (!isCancelled()) {
                bitmap = downloadImageFile(param[0]);
            }
        } catch (Exception e) {
            // Do nothing. Or just print error.
        }
        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (mActivity != null) {
            mActivity.hideProgressBar();
            mActivity.onDataFetched(bitmap);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (mActivity != null) {
            mActivity.hideProgressBar();
            mActivity.onDataCancelled();
        }
    }

    private Bitmap downloadImageFile(String url) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
package me.mitkovic.android.datafetcherasynctask.datafetching;

import android.graphics.Bitmap;

public interface IDownloadImageAsyncTaskHolder {

    void onDataFetched(Bitmap bitmap);

    void onDataCancelled();

    void showProgressBar();

    void hideProgressBar();
    
}
package me.mitkovic.android.datafetcherasynctask.datafetching;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import me.mitkovic.android.datafetcherasynctask.notcanceling.NotCancelledActivity;

public class NotCancelledAsyncTask extends AsyncTask<String, Void, Bitmap> {

    private NotCancelledActivity mActivity;

    public NotCancelledAsyncTask(NotCancelledActivity activity) {
        mActivity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mActivity.mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected Bitmap doInBackground(String... param) {
        return downloadImageFile(param[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
            mActivity.mProgressBar.setVisibility(View.GONE);
            mActivity.onDataFetched(bitmap);
    }

    private Bitmap downloadImageFile(String url) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}
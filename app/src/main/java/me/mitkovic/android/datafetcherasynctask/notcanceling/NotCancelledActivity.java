package me.mitkovic.android.datafetcherasynctask.notcanceling;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;

import me.mitkovic.android.datafetcherasynctask.R;
import me.mitkovic.android.datafetcherasynctask.datafetching.NotCancelledAsyncTask;

public class NotCancelledActivity extends AppCompatActivity {

    private static final String IMAGE_TO_DOWNLOAD_URL = "http://www.mitkovic.me/android/blog/samples/threads/img/home3.jpg";

    private NotCancelledAsyncTask mDataFetcherAsyncTask;
    public ProgressBar mProgressBar;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_standalone);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.not_cancelled_asynctask));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mImageView = (ImageView) findViewById(R.id.image_view);

        fetchData();
    }

    private void fetchData() {
        mImageView.setImageBitmap(null);

        mDataFetcherAsyncTask = new NotCancelledAsyncTask(this);
        mDataFetcherAsyncTask.execute(IMAGE_TO_DOWNLOAD_URL);
    }

    private void reloadData() {
        fetchData();
    }

    private void cancelReload() {
        //no canceling
    }

    public void onDataFetched(Bitmap bitmap) {
        if (bitmap != null) mImageView.setImageBitmap(bitmap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_reload_data) {
            reloadData();
            return true;
        }

        if (id == R.id.action_cancel_reload) {
            cancelReload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
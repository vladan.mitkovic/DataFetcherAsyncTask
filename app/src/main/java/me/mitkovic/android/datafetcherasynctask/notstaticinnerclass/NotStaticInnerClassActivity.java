package me.mitkovic.android.datafetcherasynctask.notstaticinnerclass;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import me.mitkovic.android.datafetcherasynctask.R;

public class NotStaticInnerClassActivity extends AppCompatActivity {

    private static final String IMAGE_TO_DOWNLOAD_URL = "http://www.mitkovic.me/android/blog/samples/threads/img/home3.jpg";

    private DataFetcherAsyncTask mDataFetcherAsyncTask;
    public ProgressBar mProgressBar;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_notstaticinnerclass);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.not_static_innerclass_asynctask));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mImageView = (ImageView) findViewById(R.id.image_view);

        fetchData();
    }

    private void fetchData() {
        mImageView.setImageBitmap(null);

        mDataFetcherAsyncTask = new DataFetcherAsyncTask(this);
        mDataFetcherAsyncTask.execute(IMAGE_TO_DOWNLOAD_URL);
    }

    private void reloadData() {
        mImageView.setImageBitmap(null);
        fetchData();
    }

    private void cancelReload() {
        //no canceling
    }


    public void onDataFetched(Bitmap bitmap) {
        if (bitmap != null) mImageView.setImageBitmap(bitmap);
    }

    private class DataFetcherAsyncTask extends AsyncTask<String, Void, Bitmap> {

        private NotStaticInnerClassActivity mActivity;

        public DataFetcherAsyncTask(NotStaticInnerClassActivity activity) {
            this.mActivity = activity;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mActivity.mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Bitmap doInBackground(String... param) {
            return downloadImageFile(param[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            mActivity.mProgressBar.setVisibility(View.GONE);
            mActivity.onDataFetched(bitmap);
        }

        private Bitmap downloadImageFile(String url) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_reload_data) {
            reloadData();
            return true;
        }

        if (id == R.id.action_cancel_reload) {
            cancelReload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
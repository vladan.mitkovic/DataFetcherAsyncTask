package me.mitkovic.android.datafetcherasynctask.standalone;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import me.mitkovic.android.datafetcherasynctask.R;
import me.mitkovic.android.datafetcherasynctask.datafetching.DataFetcherAsyncTask;

public class StandaloneActivity extends AppCompatActivity {

    private static final String IMAGE_TO_DOWNLOAD_URL = "http://www.mitkovic.me/android/blog/samples/threads/img/home3.jpg";

    private DataFetcherAsyncTask mDataFetcherAsyncTask;
    public ProgressBar mProgressBar;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_standalone);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.standalone_asynctask));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mImageView = (ImageView) findViewById(R.id.image_view);

        fetchData();
    }

    private void fetchData() {
        mImageView.setImageBitmap(null);

        mDataFetcherAsyncTask = new DataFetcherAsyncTask(this);
        mDataFetcherAsyncTask.execute(IMAGE_TO_DOWNLOAD_URL);
    }

    private void reloadData() {
        mImageView.setImageBitmap(null);

        if (mDataFetcherAsyncTask != null && mDataFetcherAsyncTask.getStatus() != AsyncTask.Status.RUNNING) {
            fetchData();
        }
    }

    private void cancelReload() {
        if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);

        if (mDataFetcherAsyncTask != null && mDataFetcherAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            mDataFetcherAsyncTask.cancel(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mDataFetcherAsyncTask.cancel(true);
    }


    public void onDataFetched(Bitmap bitmap) {
        if (bitmap != null) mImageView.setImageBitmap(bitmap);
    }

    public void onDataCancelled() {
        if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_reload_data) {
            reloadData();
            return true;
        }

        if (id == R.id.action_cancel_reload) {
            cancelReload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
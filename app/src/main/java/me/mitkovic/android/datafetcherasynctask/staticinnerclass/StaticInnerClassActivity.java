package me.mitkovic.android.datafetcherasynctask.staticinnerclass;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URL;

import me.mitkovic.android.datafetcherasynctask.R;

public class StaticInnerClassActivity extends AppCompatActivity {

    private static final String IMAGE_TO_DOWNLOAD_URL = "http://www.mitkovic.me/android/blog/samples/threads/img/home3.jpg";

    private DataFetcherAsyncTask mDataFetcherAsyncTask;
    public ProgressBar mProgressBar;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_staticinnerclass);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getResources().getString(R.string.static_innerclass_asynctask));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mImageView = (ImageView) findViewById(R.id.image_view);

        fetchData();
    }

    private void fetchData() {
        mImageView.setImageBitmap(null);

        mDataFetcherAsyncTask = new DataFetcherAsyncTask(new WeakReference<>(this));
        //mDataFetcherAsyncTask = new DataFetcherAsyncTask(this);
        mDataFetcherAsyncTask.execute(IMAGE_TO_DOWNLOAD_URL);
    }

    private void reloadData() {
        mImageView.setImageBitmap(null);

        if (mDataFetcherAsyncTask != null && mDataFetcherAsyncTask.getStatus() != AsyncTask.Status.RUNNING) {
            fetchData();
        }
    }

    private void cancelReload() {
        if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);

        if (mDataFetcherAsyncTask != null && mDataFetcherAsyncTask.getStatus() == AsyncTask.Status.RUNNING) {
            mDataFetcherAsyncTask.cancel(true);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mDataFetcherAsyncTask.cancel(true);
    }


    public void onDataFetched(Bitmap bitmap) {
        if (bitmap != null) mImageView.setImageBitmap(bitmap);
    }

    public void onDataCancelled() {
        if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);
    }

    private static class DataFetcherAsyncTask extends AsyncTask<String, Void, Bitmap> {

        private WeakReference<StaticInnerClassActivity> mWeakActivity;

        public DataFetcherAsyncTask(WeakReference<StaticInnerClassActivity> activity) {
            this.mWeakActivity = activity;
        }

//        public DataFetcherAsyncTask(StaticInnerClassActivity activity) {
//            this.mWeakActivity = new WeakReference<>(activity);
//        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            StaticInnerClassActivity localReferenceActivity = mWeakActivity.get();
            if (localReferenceActivity != null) {
                localReferenceActivity.mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Bitmap doInBackground(String... param) {
            Bitmap bitmap = null;
            try {
                if (!isCancelled()) {
                    bitmap = downloadImageFile(param[0]);
                }
            } catch (Exception e) {
                // Do nothing. Or just print error.
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);

            StaticInnerClassActivity localReferenceActivity = mWeakActivity.get();
            if (localReferenceActivity != null) {
                localReferenceActivity.mProgressBar.setVisibility(View.GONE);
                localReferenceActivity.onDataFetched(bitmap);
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            StaticInnerClassActivity localReferenceActivity = mWeakActivity.get();
            if (localReferenceActivity != null) {
                localReferenceActivity.mProgressBar.setVisibility(View.GONE);
                localReferenceActivity.onDataCancelled();
            }
        }

        private Bitmap downloadImageFile(String url) {
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_reload_data) {
            reloadData();
            return true;
        }

        if (id == R.id.action_cancel_reload) {
            cancelReload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}